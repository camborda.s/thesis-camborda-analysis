This repository contains all the data and analysis scripts.

The goal of the analysis is to conduct the following analyses:

1. Distribution of phenotypic variation for each trait
2. Correlation between traits
3. Principal component or related analysis to identify groups of accessions who will be later validated